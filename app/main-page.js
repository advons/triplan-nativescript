/*
In NativeScript, a file with the same name as an XML file is known as
a code-behind file. The code-behind is a great place to place your view
logic, and to set up your page’s data binding.
*/

/*
NativeScript adheres to the CommonJS specification for dealing with
JavaScript modules. The CommonJS require() function is how you import
JavaScript modules defined in other files.
*/
var frameModule = require("tns-core-modules/ui/frame");
var HomeViewModel = require("./main-view-model");
const appSettings = require("application-settings");
const httpModule = require("http");
const timerModule = require("tns-core-modules/timer");
const geoLocation = require("nativescript-geolocation");
var application = require("application");          
// var orientationModule = require("nativescript-screen-orientation");
    

var homeViewModel = new HomeViewModel();

function pageLoaded(args) {
  /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */

    var page = args.object;

    if (application.android) {
        application.android.on(application.AndroidApplication.activityBackPressedEvent, function() {});
    }

    
    // orientationModule.setCurrentOrientation("portrait",function() {
    //         console.log("portrait orientation set");
    //     });


    function getUniqueID() {
        function idGenerate() {
            function chr4(){
                return Math.random().toString(16).slice(-4);
            }
            return chr4() + chr4() +
                '-' + chr4() +
                '-' + chr4() +
                '-' + chr4() +
                '-' + chr4() + chr4() + chr4();
        }

        function strIdGenerate() {
            let tmp_id = idGenerate()
            tmp_id = tmp_id.split('-').join('')
            tmp_id = 't' + tmp_id.substring(1, tmp_id.length)
            return tmp_id
        }

        var triplan_unique_id = appSettings.getString("triplan_unique_id")
        if (!triplan_unique_id) {
            appSettings.setString("triplan_unique_id", triplan_unique_id = strIdGenerate())
            return triplan_unique_id
        } else {
            if (triplan_unique_id.length === 32) {
                return triplan_unique_id
            } else {
                appSettings.setString("triplan_unique_id", triplan_unique_id = strIdGenerate())
                return triplan_unique_id
            }
        }
    }



  /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and JavaScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
    page.bindingContext = homeViewModel;
    let userId = getUniqueID()
    page.bindingContext.initPageOnThis(page)

    httpModule.getJSON("https://triplan-190812.appspot.com/app-settings?userId=" + userId).then((data) => {

            let appUrl = data.appUrl;
            let poolingTime = (Number(data.gpsPollingInterval) || 5) * 1000;
            let gpsStoreUrl = data.gpsStoreUrl;
            console.log(appUrl)


            page.bindingContext.setAppUrl(appUrl)

            function startWatchLocation() {
                let timeId = timerModule.setInterval(() => {
                    
                    var location = geoLocation.getCurrentLocation({desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000}).
                    then(function(location) {
                        if (location) {
                            //console.log("Current location is: " + JSON.stringify(location));
                        }
                        fetch(gpsStoreUrl, {
                            method: "PUT",
                            headers: { "Content-Type": "application/json" },
                            body: JSON.stringify({userId: userId, lat: location.latitude, lng: location.longitude})
                        }).then((response) => {
                            console.log("RES:", response._bodyInit)
                        }, (e) => {
                            console.log("ERROR sending geo", e)
                        });
                    }, function(e){
                        console.log("Error: " + e.message);
                    });
                }, poolingTime);
            }

            function enableLocationServices() {
                geoLocation.isEnabled().then(enabled => {
                    if (!enabled) {
                        geoLocation.enableLocationRequest().then(() => startWatchLocation());
                    } else {
                        startWatchLocation();
                    }
                });
            }

            enableLocationServices();

            //alert(JSON.stringify(data));
        }, (e) => {
            console.log('HTTP get error, on getting app parameters.')
    });

}

/*
Exporting a function in a NativeScript code-behind file makes it accessible
to the file’s corresponding XML file. In this case, exporting the pageLoaded
function here makes the pageLoaded="pageLoaded" binding in this page’s XML
file work.
*/

//    function onNavigatingFrom(){
//         orientationModule.orientationCleanup();
//         }


exports.pageLoaded = pageLoaded;
//exports.onNavigatingFrom=onNavigatingFrom;
